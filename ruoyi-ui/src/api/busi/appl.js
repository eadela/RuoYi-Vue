import request from '@/utils/request'

// 查询报名管理列表
export function listAppl(query) {
  return request({
    url: '/busi/appl/list',
    method: 'get',
    params: query
  })
}

// 查询报名管理详细
export function getAppl(applId) {
  return request({
    url: '/busi/appl/' + applId,
    method: 'get'
  })
}

// 新增报名管理
export function addAppl(data) {
  return request({
    url: '/busi/appl',
    method: 'post',
    data: data
  })
}

// 修改报名管理
export function updateAppl(data) {
  return request({
    url: '/busi/appl',
    method: 'put',
    data: data
  })
}

// 删除报名管理
export function delAppl(applId) {
  return request({
    url: '/busi/appl/' + applId,
    method: 'delete'
  })
}

// 导出报名管理
export function exportAppl(query) {
  return request({
    url: '/busi/appl/export',
    method: 'get',
    params: query
  })
}

// 下载用户导入模板
export function importTemplate() {
  return request({
    url: '/busi/appl/importTemplate',
    method: 'get'
  })
}