package com.ruoyi.busi.service.impl;

import com.ruoyi.busi.domain.BusiApplInfo;
import com.ruoyi.busi.mapper.BusiApplInfoMapper;
import com.ruoyi.busi.service.IBusiApplInfoService;
import com.ruoyi.common.constant.ApplConstants;
import com.ruoyi.common.enums.ApplOperItemsType;
import com.ruoyi.common.enums.ApplOperType;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.identify.IdentityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 报名管理Service业务层处理
 *
 * @author coder
 * @date 2021-06-23
 */
@Service
public class BusiApplInfoServiceImpl implements IBusiApplInfoService {

    private static final Logger log = LoggerFactory.getLogger(BusiApplInfoServiceImpl.class);

    @Autowired
    private BusiApplInfoMapper busiApplInfoMapper;

    /**
     * 查询报名管理
     *
     * @param applId 报名管理ID
     * @return 报名管理
     */
    @Override
    public BusiApplInfo selectBusiApplInfoById(Long applId) {
        return busiApplInfoMapper.selectBusiApplInfoById(applId);
    }

    /**
     * 查询报名管理列表
     *
     * @param busiApplInfo 报名管理
     * @return 报名管理
     */
    @Override
    public List<BusiApplInfo> selectBusiApplInfoList(BusiApplInfo busiApplInfo) {
        return busiApplInfoMapper.selectBusiApplInfoList(busiApplInfo);
    }

    /**
     * 新增报名管理
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    @Override
    public int insertBusiApplInfo(BusiApplInfo busiApplInfo) {
        busiApplInfo.setCreateTime(DateUtils.getNowDate());
        return busiApplInfoMapper.insertBusiApplInfo(busiApplInfo);
    }

    /**
     * 修改报名管理
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    @Override
    public int updateBusiApplInfo(BusiApplInfo busiApplInfo) {
        busiApplInfo.setUpdateTime(DateUtils.getNowDate());
        return busiApplInfoMapper.updateBusiApplInfo(busiApplInfo);
    }

    /**
     * 批量删除报名管理
     *
     * @param applIds 需要删除的报名管理ID
     * @return 结果
     */
    @Override
    public int deleteBusiApplInfoByIds(Long[] applIds) {
        return busiApplInfoMapper.deleteBusiApplInfoByIds(applIds);
    }

    /**
     * 删除报名管理信息
     *
     * @param applId 报名管理ID
     * @return 结果
     */
    @Override
    public int deleteBusiApplInfoById(Long applId) {
        return busiApplInfoMapper.deleteBusiApplInfoById(applId);
    }

    /**
     * 校验报名人员种类唯一
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    @Override
    public String checkApplOperUnique(BusiApplInfo busiApplInfo) {
        int count = busiApplInfoMapper.checkApplOperUnique(busiApplInfo);
        if (count > 0) {
            return ApplConstants.NOT_UNIQUE;
        }
        return ApplConstants.UNIQUE;
    }

    /**
     * 获取种类报名人信息
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    @Override
    public BusiApplInfo getOperAppl(BusiApplInfo busiApplInfo) {
        return busiApplInfoMapper.getOperAppl(busiApplInfo);
    }

    /**
     * 获取种类报名人信息列表
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    @Override
    public List<BusiApplInfo> selectBusiApplInfoListByIdentifyAndItem(BusiApplInfo busiApplInfo) {
        return busiApplInfoMapper.selectBusiApplInfoListByIdentifyAndItem(busiApplInfo);
    }

    /**
     * 导入报名数据
     *
     * @param applList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    @Override
    public String importAppl(List<BusiApplInfo> applList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(applList) || applList.size() == 0) {
            throw new CustomException("导入报名数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (BusiApplInfo appl : applList) {
            try {
                //验证导入数据是否符合要求
                if (StringUtils.isNull(appl.getApplNm()) || appl.getApplNm().length() == 0 || appl.getApplNm().length() > 10) {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、 报名信息姓名不符合规范");
                    continue;
                }
                if (StringUtils.isNull(appl.getIdentity()) || !IdentityUtils.validate(appl.getIdentity())) {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、 " + appl.getApplNm() + " 报名信息身份证号不符合规范");
                    continue;
                }
                if (!ApplOperType.isOperExist(appl.getOperType())) {
                    System.out.println(appl.getOperType());
                    System.out.println(appl.getOperItemsType());
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、 " + appl.getApplNm() + " 报名信息作业种类不符合规范");
                    continue;
                }
                if (ApplOperType.isOperExist(appl.getOperType()) && !ApplOperItemsType.isMatch(appl.getOperType(), appl.getOperItemsType())) {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、 " + appl.getApplNm() + " 报名信息作业项目与作业种类不匹配");
                    continue;
                }

                // 验证是否存在这个报名人
                BusiApplInfo applDb = this.getOperAppl(appl);
                if (StringUtils.isNull(applDb)) {
                    appl.setCreateBy(operName);
                    this.insertBusiApplInfo(appl);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、" + appl.getApplNm() + " 报名信息导入成功");
                } else if (isUpdateSupport) {
                    appl.setUpdateBy(operName);
                    this.updateBusiApplInfo(appl);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、 " + appl.getApplNm() + " 报名信息更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、 " + appl.getApplNm() + " 报名信息已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、 " + appl.getApplNm() + " 报名信息导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
