package com.ruoyi.busi.service;

import com.ruoyi.busi.domain.BusiApplInfo;

import java.util.List;

/**
 * 报名管理Service接口
 * 
 * @author coder
 * @date 2021-06-23
 */
public interface IBusiApplInfoService 
{
    /**
     * 查询报名管理
     * 
     * @param applId 报名管理ID
     * @return 报名管理
     */
    public BusiApplInfo selectBusiApplInfoById(Long applId);

    /**
     * 查询报名管理列表
     * 
     * @param busiApplInfo 报名管理
     * @return 报名管理集合
     */
    public List<BusiApplInfo> selectBusiApplInfoList(BusiApplInfo busiApplInfo);

    /**
     * 新增报名管理
     * 
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public int insertBusiApplInfo(BusiApplInfo busiApplInfo);

    /**
     * 修改报名管理
     * 
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public int updateBusiApplInfo(BusiApplInfo busiApplInfo);

    /**
     * 批量删除报名管理
     * 
     * @param applIds 需要删除的报名管理ID
     * @return 结果
     */
    public int deleteBusiApplInfoByIds(Long[] applIds);

    /**
     * 删除报名管理信息
     * 
     * @param applId 报名管理ID
     * @return 结果
     */
    public int deleteBusiApplInfoById(Long applId);

    /**
     * 校验报名人员种类唯一
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public String checkApplOperUnique(BusiApplInfo busiApplInfo);

    /**
     * 获取种类报名人信息
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public BusiApplInfo getOperAppl(BusiApplInfo busiApplInfo);

    /**
     * 获取种类报名人信息列表
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public List<BusiApplInfo> selectBusiApplInfoListByIdentifyAndItem(BusiApplInfo busiApplInfo);

    /**
     * 导入用户数据
     *
     * @param applList 报名数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importAppl(List<BusiApplInfo> applList, Boolean isUpdateSupport, String operName);
}
