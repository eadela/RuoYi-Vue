package com.ruoyi.busi.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 报名管理对象 busi_appl_info
 *
 * @author coder
 * @date 2021-06-23
 */
public class BusiApplInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long applId;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String applNm;

    /**
     * 性别（0男 1女 2未知）
     */
    @Excel(name = "性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /**
     * 身份证号
     */
    @Excel(name = "身份证号", width = 18)
    private String identity;

    /**
     * 报名日期
     */
    @JsonFormat(pattern = "yyyy.MM.dd")
    @Excel(name = "报名日期", width = 30, dateFormat = "yyyy.MM.dd")
    private Date applTime;

    /**
     * 学历（字典项busi_appl_edu）
     */
    @Excel(name = "学历", dictType = "busi_appl_edu")
    private String eduType;

    /**
     * 作业种类（字典项busi_oper_type）
     */
    @Excel(name = "作业种类", dictType = "busi_oper_type", width = 30)
    private String operType;

    /**
     * 作业项目（字典项 busi_oper_items_type）
     */
    @Excel(name = "作业项目", dictType = "busi_oper_items_type", width = 30)
    private String operItemsType;

    /**
     * 理论成绩
     */
    private Integer theoryScores;

    /**
     * 实操成绩
     */
    private Integer practiceScores;

    /**
     * 单位
     */
    @Excel(name = "单位", width = 50)
    private String company;

    /**
     * 地址
     */
    @Excel(name = "地址", width = 50)
    private String addr;

    /**
     * 联系电话1
     */
    @Excel(name = "联系电话1")
    private String phone;

    /**
     * 联系电话2
     */
    @Excel(name = "联系电话2")
    private String tel;

    /**
     * 缴费情况
     */
    @Excel(name = "缴费情况")
    private String payment;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private String leader;

    /**
     * 负责人联系电话
     */
    @Excel(name = "负责人联系电话", type = Excel.Type.IMPORT)
    private String leaderPhone;

    /**
     * 通知情况
     */
    @Excel(name = "通知情况", width = 50)
    private String remark;


    private List<String> paymentQryList;

    public void setApplId(Long applId) {
        this.applId = applId;
    }

    public Long getApplId() {
        return applId;
    }

    public void setApplNm(String applNm) {
        this.applNm = applNm;
    }

    public String getApplNm() {
        return applNm;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getIdentity() {
        return identity;
    }

    public void setApplTime(Date applTime) {
        this.applTime = applTime;
    }

    public Date getApplTime() {
        return applTime;
    }

    public void setEduType(String eduType) {
        this.eduType = eduType;
    }

    public String getEduType() {
        return eduType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }

    public String getOperType() {
        return operType;
    }

    public void setOperItemsType(String operItemsType) {
        this.operItemsType = operItemsType;
    }

    public String getOperItemsType() {
        return operItemsType;
    }

    public void setTheoryScores(Integer theoryScores) {
        this.theoryScores = theoryScores;
    }

    public Integer getTheoryScores() {
        return theoryScores;
    }

    public void setPracticeScores(Integer practiceScores) {
        this.practiceScores = practiceScores;
    }

    public Integer getPracticeScores() {
        return practiceScores;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompany() {
        return company;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getAddr() {
        return addr;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel() {
        return tel;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPayment() {
        return payment;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeaderPhone(String leaderPhone) {
        this.leaderPhone = leaderPhone;
    }

    public String getLeaderPhone() {
        return leaderPhone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<String> getPaymentQryList() {
        return paymentQryList;
    }

    public void setPaymentQryList(List<String> paymentQryList) {
        this.paymentQryList = paymentQryList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("applId", getApplId())
                .append("applNm", getApplNm())
                .append("sex", getSex())
                .append("identity", getIdentity())
                .append("applTime", getApplTime())
                .append("eduType", getEduType())
                .append("operType", getOperType())
                .append("operItemsType", getOperItemsType())
                .append("theoryScores", getTheoryScores())
                .append("practiceScores", getPracticeScores())
                .append("company", getCompany())
                .append("addr", getAddr())
                .append("phone", getPhone())
                .append("tel", getTel())
                .append("payment", getPayment())
                .append("leader", getLeader())
                .append("leaderPhone", getLeaderPhone())
                .append("remark", getRemark())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
