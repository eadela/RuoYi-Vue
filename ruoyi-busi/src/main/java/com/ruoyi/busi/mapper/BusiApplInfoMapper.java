package com.ruoyi.busi.mapper;

import com.ruoyi.busi.domain.BusiApplInfo;

import java.util.List;

/**
 * 报名管理Mapper接口
 * 
 * @author coder
 * @date 2021-06-23
 */
public interface BusiApplInfoMapper 
{
    /**
     * 查询报名管理
     * 
     * @param applId 报名管理ID
     * @return 报名管理
     */
    public BusiApplInfo selectBusiApplInfoById(Long applId);

    /**
     * 查询报名管理列表
     * 
     * @param busiApplInfo 报名管理
     * @return 报名管理集合
     */
    public List<BusiApplInfo> selectBusiApplInfoList(BusiApplInfo busiApplInfo);

    /**
     * 新增报名管理
     * 
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public int insertBusiApplInfo(BusiApplInfo busiApplInfo);

    /**
     * 修改报名管理
     * 
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public int updateBusiApplInfo(BusiApplInfo busiApplInfo);

    /**
     * 删除报名管理
     * 
     * @param applId 报名管理ID
     * @return 结果
     */
    public int deleteBusiApplInfoById(Long applId);

    /**
     * 批量删除报名管理
     * 
     * @param applIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusiApplInfoByIds(Long[] applIds);

    /**
     * 校验报名人员种类唯一
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public int checkApplOperUnique(BusiApplInfo busiApplInfo);

    /**
     * 获取种类报名人信息
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public BusiApplInfo getOperAppl(BusiApplInfo busiApplInfo);

    /**
     * 获取种类报名人信息列表
     *
     * @param busiApplInfo 报名管理
     * @return 结果
     */
    public List<BusiApplInfo> selectBusiApplInfoListByIdentifyAndItem(BusiApplInfo busiApplInfo);
}
