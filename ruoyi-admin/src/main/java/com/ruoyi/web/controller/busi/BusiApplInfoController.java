package com.ruoyi.web.controller.busi;

import com.ruoyi.busi.domain.BusiApplInfo;
import com.ruoyi.busi.service.IBusiApplInfoService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.ApplConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.ApplOperItemsType;
import com.ruoyi.common.enums.ApplOperType;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 报名管理Controller
 *
 * @author coder
 * @date 2021-06-23
 */
@RestController
@RequestMapping("/busi/appl")
public class BusiApplInfoController extends BaseController {
    @Autowired
    private IBusiApplInfoService busiApplInfoService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询报名管理列表
     */
    @PreAuthorize("@ss.hasPermi('busi:appl:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusiApplInfo busiApplInfo) {
        startPage();
        List<BusiApplInfo> list = busiApplInfoService.selectBusiApplInfoList(busiApplInfo);
        return getDataTable(list);
    }

    /**
     * 导出报名管理列表
     */
    @PreAuthorize("@ss.hasPermi('busi:appl:export')")
    @Log(title = "报名管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusiApplInfo busiApplInfo) {
        List<BusiApplInfo> list = busiApplInfoService.selectBusiApplInfoList(busiApplInfo);
        //将身份证号部分变为*******
        for (BusiApplInfo appl : list) {
            appl.setIdentity(appl.getIdentity().substring(0, 6) + "********" + appl.getIdentity().substring(14));
        }
        ExcelUtil<BusiApplInfo> util = new ExcelUtil<BusiApplInfo>(BusiApplInfo.class);
        return util.exportExcel(list, "报名人员数据");
    }

    /**
     * 获取报名管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('busi:appl:query')")
    @GetMapping(value = "/{applId}")
    public AjaxResult getInfo(@PathVariable("applId") Long applId) {
        return AjaxResult.success(busiApplInfoService.selectBusiApplInfoById(applId));
    }

    /**
     * 新增报名管理
     */
    @PreAuthorize("@ss.hasPermi('busi:appl:add')")
    @Log(title = "报名管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusiApplInfo busiApplInfo) {
        if (!ApplOperType.isOperExist(busiApplInfo.getOperType())) {
            return AjaxResult.error("新增报名'" + busiApplInfo.getApplNm() + "'失败，报名信息作业种类不符合规范");
        }
        if (ApplOperType.isOperExist(busiApplInfo.getOperType()) && !ApplOperItemsType.isMatch(busiApplInfo.getOperType(), busiApplInfo.getOperItemsType())) {
            return AjaxResult.error("新增报名'" + busiApplInfo.getApplNm() + "'失败，报名信息作业项目与作业种类不匹配");
        }
        if (ApplConstants.NOT_UNIQUE.equals(busiApplInfoService.checkApplOperUnique(busiApplInfo))) {
            return AjaxResult.error("新增报名'" + busiApplInfo.getApplNm() + "'失败，报名种类已存在");
        }
        busiApplInfo.setCreateBy(SecurityUtils.getUsername());
        return toAjax(busiApplInfoService.insertBusiApplInfo(busiApplInfo));
    }

    /**
     * 修改报名管理
     */
    @PreAuthorize("@ss.hasPermi('busi:appl:edit')")
    @Log(title = "报名管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusiApplInfo busiApplInfo) {
        if (!ApplOperType.isOperExist(busiApplInfo.getOperType())) {
            return AjaxResult.error("修改报名'" + busiApplInfo.getApplNm() + "'失败，报名信息作业种类不符合规范");
        }
        if (ApplOperType.isOperExist(busiApplInfo.getOperType()) && !ApplOperItemsType.isMatch(busiApplInfo.getOperType(), busiApplInfo.getOperItemsType())) {
            return AjaxResult.error("修改报名'" + busiApplInfo.getApplNm() + "'失败，报名信息作业项目与作业种类不匹配");
        }
        // 通过身份证号和作业种类，获取是否已存在，注意要比较id不可以相同，代表不是同一条数据
        List<BusiApplInfo> list = busiApplInfoService.selectBusiApplInfoListByIdentifyAndItem(busiApplInfo);
        for (BusiApplInfo appl : list) {
            if (appl.getApplId() != busiApplInfo.getApplId()) {
                return AjaxResult.error("修改报名'" + busiApplInfo.getApplNm() + "'失败，报名种类已存在");
            }
        }
        busiApplInfo.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(busiApplInfoService.updateBusiApplInfo(busiApplInfo));
    }

    /**
     * 删除报名管理
     */
    @PreAuthorize("@ss.hasPermi('busi:appl:remove')")
    @Log(title = "报名管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{applIds}")
    public AjaxResult remove(@PathVariable Long[] applIds) {
        return toAjax(busiApplInfoService.deleteBusiApplInfoByIds(applIds));
    }

    /**
     * 报名管理模版下载
     */
    @GetMapping("/importTemplate")
    public AjaxResult importTemplate() {
        ExcelUtil<BusiApplInfo> util = new ExcelUtil<BusiApplInfo>(BusiApplInfo.class);
        return util.importTemplateExcel("报名模板");
    }

    @Log(title = "报名管理", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('busi:appl:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<BusiApplInfo> util = new ExcelUtil<BusiApplInfo>(BusiApplInfo.class);
        List<BusiApplInfo> applList = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String message = busiApplInfoService.importAppl(applList, updateSupport, loginUser.getUsername());
        return AjaxResult.success(message);
    }
}
