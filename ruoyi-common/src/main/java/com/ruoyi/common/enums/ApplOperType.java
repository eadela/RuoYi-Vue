package com.ruoyi.common.enums;

public enum ApplOperType {
    EP("电梯作业", "1"),
    BP("锅炉作业", "2"),
    BO("起重机作业", "3"),
    PV("压力容器作业", "4"),
    SM("场（厂）内专用机动车辆作业", "5"),
    SE("特种设备安全管理", "6"),
    SA("安全附件维修作业", "7"),
    CO("气瓶作业", "8");


    // 成员变量
    private String name;
    private String type;

    ApplOperType(String name, String type) {
        this.name = name;
        this.type = type;
    }

    // 校验是否有值满足
    public static Boolean isOperExist(String type) {
        for (ApplOperType oper : ApplOperType.values()) {
            if (oper.getType().equals(type)) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
