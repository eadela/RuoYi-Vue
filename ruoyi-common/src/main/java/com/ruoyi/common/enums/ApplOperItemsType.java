package com.ruoyi.common.enums;

import static com.ruoyi.common.enums.ApplOperType.*;

public enum ApplOperItemsType {
    ERA("电梯修理（T）", "1"),
    IBS("工业锅炉司炉（G1）", "2"),
    UBS("电站锅炉司炉（G1）", "3"),
    BWT("锅炉水处理（G3）", "4"),
    CCA("起重机指挥（Q1）", "5"),
    CDG("起重机司机(Q2)", "6"),
    OQO("快开门式压力容器操作（R1）", "7"),
    FPV("移动式压力容器充装（R2）", "8"),
    OCM("氧舱维护保养（R3）", "9"),
    FDA("叉车司机（N1）", "10"),
    TBT("观光车和观光列车司机（N2）", "11"),
    SEA("特种设备安全管理（A）", "12"),
    SVC("安全阀校验（F）", "13"),
    CFA("气瓶充装（P）", "14");

    // 成员变量
    private String name;
    private String type;

    ApplOperItemsType(String name, String type) {
        this.name = name;
        this.type = type;
    }

    // 校验作业种类和作业项目是否匹配
    public static Boolean isMatch(String operType, String itemType) {
        if (EP.getType().equals(operType) && ERA.getType().equals(itemType)) {
            return true;
        } else if (BP.getType().equals(operType) && IBS.getType().equals(itemType)) {
            return true;
        } else if (BP.getType().equals(operType) && UBS.getType().equals(itemType)) {
            return true;
        } else if (BP.getType().equals(operType) && BWT.getType().equals(itemType)) {
            return true;
        } else if (BO.getType().equals(operType) && CCA.getType().equals(itemType)) {
            return true;
        } else if (BO.getType().equals(operType) && CDG.getType().equals(itemType)) {
            return true;
        } else if (PV.getType().equals(operType) && OQO.getType().equals(itemType)) {
            return true;
        } else if (PV.getType().equals(operType) && FPV.getType().equals(itemType)) {
            return true;
        } else if (PV.getType().equals(operType) && OCM.getType().equals(itemType)) {
            return true;
        } else if (SM.getType().equals(operType) && FDA.getType().equals(itemType)) {
            return true;
        } else if (SM.getType().equals(operType) && TBT.getType().equals(itemType)) {
            return true;
        } else if (SE.getType().equals(operType) && SEA.getType().equals(itemType)) {
            return true;
        } else if (SA.getType().equals(operType) && SVC.getType().equals(itemType)) {
            return true;
        } else if (CO.getType().equals(operType) && CFA.getType().equals(itemType)) {
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
